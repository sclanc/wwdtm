# WWDTM: A clear rip off of NPR's Wait Wait Don't Tell Me, the NPR News Quiz
## A fun (not for profit) venture into webscraping and lamda functions with python

The goal is to make Google Home and Amazon Echo applications that simulate the fun of quizing yourself on the most
current news, with questions generated as fast as headlines are published across the web.
---

**File Functions**

Headlines.py: grabing the headlines, formating them, and saving them to a database  
Questions.py: groups questions by topic and finds the best ones for the application, removing redundancies  
---

**NLTK Notes** 

    GRAMMAR TAGSET = {
        'CC':   'Coordinating conjunction',   'PRP$': 'Possessive pronoun',
        'CD':   'Cardinal number',            'RB':   'Adverb',
        'DT':   'Determiner',                 'RBR':  'Adverb, comparative',
        'EX':   'Existential there',          'RBS':  'Adverb, superlative',
        'FW':   'Foreign word',               'RP':   'Particle',
        'JJ':   'Adjective',                  'TO':   'to',
        'JJR':  'Adjective, comparative',     'UH':   'Interjection',
        'JJS':  'Adjective, superlative',     'VB':   'Verb, base form',
        'LS':   'List item marker',           'VBD':  'Verb, past tense',
        'MD':   'Modal',                      'NNS':  'Noun, plural',
        'NN':   'Noun, singular or masps',    'VBN':  'Verb, past participle',
        'VBZ':  'Verb,3rd ps. sing. present', 'NNP':  'Proper noun, singular',
        'NNPS': 'Proper noun plural',         'WDT':  'wh-determiner',
        'PDT':  'Predeterminer',              'WP':   'wh-pronoun',
        'POS':  'Possessive ending',          'WP$':  'Possessive wh-pronoun',
        'PRP':  'Personal pronoun',           'WRB':  'wh-adverb',
        '(':    'open parenthesis',           ')':    'close parenthesis',
        '``':   'open quote',                 ',':    'comma',
        "''":   'close quote',                '.':    'period',
        '#':    'pound sign (currency marker)',
        '$':    'dollar sign (currency marker)',
        'IN':   'Preposition/subord. conjunction',
        'SYM':  'Symbol (mathematical or scientific)',
        'VBG':  'Verb, gerund/present participle',
        'VBP':  'Verb, non-3rd ps. sing. present',
        ':':    'colon',
        }

        chunk_types=['LOCATION', 'ORGANIZATION', 'PERSON', 'DURATION', 'DATE', 'CARDINAL', 'PERCENT', 'MONEY', 'MEASURE'], root_label='S'
