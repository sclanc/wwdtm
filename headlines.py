import urllib.request
from bs4 import BeautifulSoup
import sys
def getHeadlines(args): 
    headlines = []
    for arg in args:
        try:
            url = arg
            req = urllib.request.Request(url)
            response = urllib.request.urlopen(req)
            html = response.read()
        except urllib.error.URLError as e:
            print(e)
        else:
            if "wikipedia" in arg:
                headlines += wikipedia_scrape(html)
            elif "reuters" in arg:
                headlines += reuters_scrape(html)
            # elif "fox" etc...
    return headlines
def wikipedia_scrape(html):
            bsObj = BeautifulSoup(html, "html.parser")
            headlinesParent = bsObj.find('div', attrs={'id': 'mp-itn'}).find('ul')
            headlinesNodes = headlinesParent.find_all('li')
            headlines = [node.get_text() for node in headlinesNodes]
            return headlines
def reuters_scrape(html):
    bsObj = BeautifulSoup(html, "html.parser")
    headlines = bsObj.find_all('article')
    headlines = [headline.get_text() for headline in headlines]
    for headline in headlines:
        print(headline)
