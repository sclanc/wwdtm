from question import question
import headlines
import sys
import numpy
import nltk
import re

def formatQuestion(qline, child):
    answer = re.sub('(\/\w+)|\([A-Z]+|\)','',str(child))
    qline = re.sub(answer, ' ___', qline)
    q = question(qline, answer)
    return q

def main():
    args = []
    args = sys.argv
    args.pop(0)
    imports = headlines.getHeadlines(args)
    questions = []
    qline = ''
    for headline in imports:
        qline = headline
        tokens = nltk.word_tokenize(headline)
        tagged = nltk.pos_tag(tokens)
        tree = nltk.chunk.ne_chunk(tagged)
        for child in tree:
            if type(child) is nltk.tree.Tree:
                if child.label() in ['LOCATION', 'ORGANIZATION', 'PERSON']:
                    questions.append(formatQuestion(qline, child))
    for question in questions:
        question.printQuestion()



    
if __name__ == "__main__":
    main()


